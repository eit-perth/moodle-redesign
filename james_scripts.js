// Launch the admin tools whenever
domAdminMode();

// Launch the ODS to Excel script
odsToExcel();

// Copyright script
copyrightUpdate();

// Launch the button to top script
buttonToTop();

// If the page is a general course, then launch the functions 
if (document.querySelector(".pagelayout-course") !== null) {
    //summaryTextReposition();
    navMenuMobile();
}

// Only launch if the login page
if (document.querySelector("#page-login-index") !== null) {
    loginBackgroundRotation();
}

// Don't launch on the login page
if (document.querySelector("#page-login-index") == null) {
    // UTC timezone stamp 
    utcTimeStamp();
}


// If the page is editing the grade category, then launch the functions
if (document.querySelector(".path-grade-edit.category-1") !== null) {
    setTimeout(expandShowMore, 2000);
}

// If the page is the dashboard, then launch the functions
if (document.querySelector("body.pagelayout-mydashboard") !== null) {
    emailAddresstoLocal();
    // timezoneDetector();
}


/*





*/

/*==================================
Email Address to Local Storage
==================================*/
/*
Email address added to local storage.
*/
function emailAddresstoLocal() {
    // Get the email address of user
    let emailAddress = document.querySelector(".myprofileitem.city a").innerText;

    //Filter out the unecessary HTML
    emailAddress = emailAddress.split(': ').pop();

    // Add the email to local storage
    localStorage.setItem("Email:", emailAddress);
}




/*==================================
Dashboard Timezone Converter
==================================*/
/*
The below timezone converter takes the Moodle user's timezone and displays it on the main dashboard. The user's detected time zone will also be displayed. The function is called within the dashboard.
*/
function timezoneDetector() {
    // XMLHttpRequest to check for the current time zone set in settings
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            // Add the time zone page to variable
            let timezone = this.responseText;

            // Refine to the list
            timezone = timezone.split('class="col-form-label d-inline " for="id_timezone"').pop();
            timezone = timezone.split('</select>')[0];

            // Refine to the specific time zone 
            timezone = timezone.split('" selected').pop();
            timezone = timezone.split('</option>')[0];

            // Issues with spacing - had to cut off the ">" in the selection of the country
            timezone = timezone.split('>').pop();

            // Add time zone to local storage
            localStorage.setItem("Timezone", timezone);

            // Add the timezone to the dashboard
            document.getElementById("insert_timezone").innerHTML = timezone;

            // Detect the user's time zone
            let detectedTimezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

            // Add the detected time zone to the user's page
            document.getElementById("insert_detected_timezone").innerHTML = detectedTimezone;

            // Detect if time zones are the same 
            if (detectedTimezone !== timezone) {
                document.querySelector(".timezoneIncorrect").style.display = "block";
            }

        }
    };
    xhttp.open("GET", "https://moodle.eit.edu.au/user/editadvanced.php", true);
    xhttp.send();

}



/*==================================
Dashboard Timezone Converter
==================================*/
/*
This script updates the Copyright year on Moodle.
*/
function copyrightUpdate() {
    document.querySelector("#copyright-date").innerHTML = new Date().getFullYear();
}





/*==================================
Summary Text Repositioning
==================================*/
/* 
The below code will reposition the summary text within the collapsible topic. 
*/
function summaryTextReposition() {
    // Getting the old summary text HTML
    let oldSummaryText = document.querySelectorAll(".summary.summaryalwaysshown");

    // Identifying the new summary text position
    let newLocation = document.querySelectorAll(".sectionbody.toggledsection");

    // Loop through all the topics
    for (let i = 1; i < oldSummaryText.length; i++) {
        if (newLocation !== null) {
            // Declaration of new summary text
            let newSummaryText;

            // Assign the old summary text content to the new summary text content 
            newSummaryText = oldSummaryText[i].innerHTML;

            // Update the summary text HTML; wrapped around a class div for further control
            newLocation[i].insertAdjacentHTML('afterbegin', '<div class="newSummaryText">' + newSummaryText + "</div>");
        }
    }


}















/*==================================
ODS to Excel Export
==================================*/
/*
This script redirects a user trying to export a gradebook from Moodle. ODS is the default selection, but the script will redirect to Excel instead.

This was requested by Jacqui Veness on the 20-Nov-2019

*/
function odsToExcel() {
    // Check that ODS is selected
    if (window.location.href.includes("/export/ods/") == true) {

        // Determine the current URL
        let excelExport = window.location.href;

        // Redirect to the Excel export instead of the ODS export
        excelExport = excelExport.replace("ods", "xls");

        // Redirect to the Excel tab instead
        window.location.href = excelExport;
    }
}




/*==================================
DOM Admin Mode
==================================*/
/* 
The below code is a set of functions that performs some DOM manipulations for admins. The code is comprised of:
    - Validating the role of the manager.
    - Rounding grades for attendence. 
    - Sorting the drop down list for the user selection
    - Override the search function on the top right with the course search. 
*/
function domAdminMode() {

    // Enable the DOM interface if DOM Admin enabled
    if (localStorage.getItem("DOM Admin") == "Enabled") {
        // Launch admin tools
        domInterface();
    } else {
        // Detect the user's email from the dashboard only
        if (document.querySelector("body.pagelayout-mydashboard") !== null) {
            detectUser();
        }
    }

    // Detect the user to launch the tools
    function detectUser() {
        // Get the email details
        let emailAddress = document.querySelector(".myprofileitem.city a").innerHTML;

        // Remove all text before email
        emailAddress = emailAddress.split('">').pop();

        // Remove all text after email
        emailAddress = emailAddress.split('</a>')[0];

        // XMLHttpRequest to check for current managers, and validate your email address against these
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                let managerInterface = this.responseText;

                // Refine the information
                managerInterface = managerInterface.split('<optgroup label="Existing users (2)">').pop();
                managerInterface = managerInterface.split('</optgroup>')[0];

                // Check that the manager exists in the list
                if (managerInterface.includes(emailAddress) == true) {
                    // Apply DOM Admin to User
                    localStorage.setItem('DOM Admin', 'Enabled');
                    console.log("Correct access");
                    location.reload();
                }

            }
        };
        xhttp.open("GET", "https://moodle.eit.edu.au/admin/roles/assign.php?contextid=1&roleid=1", true);
        xhttp.send();

    }

    // Enable the DOM interface
    function domInterface() {
        // document.querySelector(".fixed-top.navbar").insertAdjacentHTML("afterend", '<a href="#">test</a>');
        //<i class="fas fa-screwdriver"></i>
        // Insert div on the page
        document.querySelector("body").insertAdjacentHTML("afterbegin", '<div class="domAdminPanel"></div>');
            
        // Check if single view grades page and launch Grade Rounder
        if (document.querySelector("body.path-grade-report-singleview") !== null) {
            gradeRounder();
        }

        // Check if overview report page and launch name sort function
        if (document.querySelector("#page-grade-report-overview-index") !== null) {
            invokeSort();
        }

        // Check if overview report page and launch name sort function
        if (document.querySelector("#page-login-index") == null) {
            courseSearchOverride();
        }

        // Check if course Moodle page and launch the quick selection menu
            quickSelectionMenu();
        




    }

    // Grade Rounder
    function gradeRounder() {

        let entireDoc = document.querySelector('.domAdminPanel').innerHTML = '<button class="checkData">Check Fields to be Changed</button><button class="extensionButton overwriteData" style="display: none;">Overwrite Changes</button>';

        // Launch confirmation check on select
        document.querySelector(".checkData").addEventListener("click", confirmationCheck);
        // Launch grade rounder on select
        document.querySelector(".overwriteData").addEventListener("click", gradeCorrector);

        // Confirm with user that they are correcting the right grades
        function confirmationCheck() {
            console.log("Confirmation Check Worked!");

            // Highlight the specific table
            let fieldsToBeChanged = document.querySelectorAll("[tabindex='1']");

            // Loop through all the the fields that will be changed
            let i;
            for (i = 0; i < fieldsToBeChanged.length; i++) {
                fieldsToBeChanged[i].style.backgroundColor = "purple";
                fieldsToBeChanged[i].style.color = "white";
            }

            document.querySelector("body").insertAdjacentHTML('afterend', '<style>.overwriteData {display: block !important;} .checkData {display: none !important;}</style>');
        }

        // Function to start the algorithm
        function gradeCorrector() {
            let gradeUpdater = document.querySelectorAll("[tabindex='1']");

            // Loop through all items in the array
            let i;
            for (i = 0; i < gradeUpdater.length; i++) {
                // If over 67 %, then give 100%
                if (gradeUpdater[i].value > 66.7) {
                    gradeUpdater[i].value = "100";
                    // If under 67% then give 0%
                } else {
                    gradeUpdater[i].value = "0";
                }
            }
        }

    }

    // Check if the grades module is active
    function invokeSort() {
        // Create the button to sort by first name
        let entireDoc = document.querySelector('.domAdminPanel').insertAdjacentHTML('afterbegin', '<button id="sortlist" class="bttn sortAlph">Sort First Name</button>');

        // Listen for the button click on the above
        document.querySelector(".sortAlph").addEventListener("click", sortListAlph);

        // Sort function
        function sortListAlph() {
            // Target the user list
            var cl = document.querySelector('#choosegradeuser select');

            // Style the element targetted to indicate it is being amended
            cl.style.background = "purple";
            cl.style.color = "white";

            // Add the list to an array
            var clTexts = new Array();

            // Sort function used from https://stackoverflow.com/questions/5248189/sort-select-menu-alphabetically/5248918#5248918
            for (i = 1; i < cl.length; i++) {
                clTexts[i - 1] = cl.options[i].text.toUpperCase() + "," + cl.options[i].text + "," + cl.options[i].value + "," + cl.options[i].selected;
            }

            clTexts.sort();

            for (i = 1; i < cl.length; i++) {
                var parts = clTexts[i - 1].split(',');

                cl.options[i].text = parts[1];
                cl.options[i].value = parts[2];
                if (parts[3] == "true") {
                    cl.options[i].selected = true;
                } else {
                    cl.options[i].selected = false;
                }
            }
        }

    }

    // Override the current search option on the top right
    function courseSearchOverride() {
        document.querySelector(".navbar-nav .d-lg-block").innerHTML = '<form action="https://moodle.eit.edu.au/course/search.php" method="get" class="form-inline" style="margin-right: 15px;"> <fieldset class="coursesearchbox invisiblefieldset"> <input name="search" type="text" size="12" value="" class="form-control"> <button class="btn btn-secondary" type="submit">Go</button></fieldset></form>';
    }

    // Quick selection menu for popular course page components
    function quickSelectionMenu() {

        // Get URL of the Moodle Course
        let currentLocation = window.location.href;

        // Remove the prefix of the URL
        currentLocation = currentLocation.split('?id=').pop();

        // Create the "users" menu item
        let userMenuItem = '<a href="https://moodle.eit.edu.au/user/index.php?id=' + currentLocation + '">Users</a> | ';

        // Create the "grades" menu item
        let gradeMenuItem = '<a href="https://moodle.eit.edu.au/grade/report/index.php?id=' + currentLocation + '">Grades</a> | ';

        // Create the "attendance" menu item
        let assignmentMenuItem = '<a href="https://moodle.eit.edu.au/mod/assign/index.php?id=' + currentLocation + '">Assignments</a> | ';

        // Create the "attendance" menu item
        let attendanceMenuItem = '<a href="https://moodle.eit.edu.au/mod/attendance/index.php?id=' + currentLocation + '">Attendance</a> | ';

        // Create the "forum" menu item
        let forumMenuItem = '<a href="https://moodle.eit.edu.au/mod/forum/index.php?id=' + currentLocation + '">Forum</a> | ';

        // Create the "quizzes" menu item
        let quizMenuItem = '<a href="http://moodle.eit.edu.au/mod/quiz/index.php?id=' + currentLocation + '">Quizzes</a> | ';

        // Create the "Turnitin" menu item
        let turnitinMenuItem = '<a href="http://moodle.eit.edu.au/mod/turnitintooltwo/index.php?id=' + currentLocation + '">Turnitin</a>';

        // Combine all the menu items
        let quickSelectionMenu = '<div class="quickSelectionMenu">' + '<strong>Quick Selection Menu</strong>' + '<br>' + userMenuItem + gradeMenuItem + assignmentMenuItem + attendanceMenuItem + forumMenuItem + quizMenuItem + turnitinMenuItem + '<br><hr>' + '</div>';

        // Insert the nav menu onto the page
        document.querySelector('#region-main').insertAdjacentHTML('beforebegin', quickSelectionMenu);

    }


}














/*==================================
Expand Show More
==================================*/
/*
The below code expands all the content when editing a category. It is a fairly minor script and its scope is only for grades.

This was requested by Megan Kellett on the 29-Nov-2019

*/


function expandShowMore() {
    let showMores = document.querySelectorAll(".moreless-toggler");
    console.log(showMores);
    for (i = 0; i < showMores.length; i++) {
        showMores[i].click();
        console.log("second");
    }
}




/*==================================
Button to Top
==================================*/
/*
The below button will stay on the page, and push the user to the top of the page. It was requested by Paul on the 11-Dec-2019.
*/
function buttonToTop() {

    let buttonToTop = document.getElementsByTagName("BODY")[0];
    buttonToTop.insertAdjacentHTML('beforeend', '<a class="back-to-top" style="z-index: 9999;background: #f8f9fa40;font-size: 150%;border: 1px solid rgba(0, 0, 0, 0.1);width: 36px;text-align: center;position: fixed;bottom: 30px;right: 40px;display: inline;" href="#"><i class="fa fa-angle-up "></i></a>');

}







/*==================================
Login Background Image Rotation
==================================*/
/*
The below script will rotate the background image om the login page between three images. 
*/
function loginBackgroundRotation() {
    // Get the background image links
    let backgroundOne = "https://eitcdn.eit.edu.au/moodle-redesign-v2/elements/loginbackground/background1.jpg";
    let backgroundTwo = "https://eitcdn.eit.edu.au/moodle-redesign-v2/elements/loginbackground/background2.jpg";
    let backgroundThree = "https://eitcdn.eit.edu.au/moodle-redesign-v2/elements/loginbackground/background3.jpg";
    let backgroundFour = "https://eitcdn.eit.edu.au/moodle-redesign-v2/elements/loginbackground/background4.jpg";
    let backgroundFive = "https://eitcdn.eit.edu.au/moodle-redesign-v2/elements/loginbackground/background5.jpg";
    let backgroundSix = "https://eitcdn.eit.edu.au/moodle-redesign-v2/elements/loginbackground/background6.jpg";


    // Get the background image element
    let backgroundTemplate = document.querySelector("#page-login-index #region-main-box");

    // Template var for background final selection
    let backgroundSelection;

    // Random num generator for background image
    let randomBackground = Math.floor(Math.random() * 6) + 1;

    // Determine the background image
    if (randomBackground == 1) {
        backgroundSelection = backgroundOne;
    } else if (randomBackground == 2) {
        backgroundSelection = backgroundTwo;
    } else if (randomBackground == 3) {
        backgroundSelection = backgroundThree;
    } else if (randomBackground == 4) {
        backgroundSelection = backgroundFour;
    } else if (randomBackground == 5) {
        backgroundSelection = backgroundFive;
    } else {
        backgroundSelection = backgroundSix;
    }

    // Implement the background image
    backgroundTemplate.style.backgroundImage = "url('" + backgroundSelection + "')";

}




/*==================================
Append Nav Menu to Header
==================================*/
/*
The below script will append the main navigation menu to the top of the page in mobile.
*/
function navMenuMobile() {
    // Assign window size for mobile
    let windowSizeMobile = window.matchMedia("(max-width: 767px)")

    if (windowSizeMobile.matches) {
        // Add the nav menu to the page
        let navMenu = document.querySelector(".block_navigation").outerHTML;

        // Append the Navigation menu to the top of the page
        document.getElementById('page-header').insertAdjacentHTML('afterend', navMenu);

        // Remove the old nav menu
        document.querySelectorAll(".block_navigation")[1].remove();
    }
}




/*==================================
Add UTC Timezone stamp to Moodle
==================================*/
/*
A timezone stamp with the UTC time has been added to the top right of Moodle (next to the search function)
*/
function utcTimeStamp() {
    // Initiate the time object
     let initiateDate = new Date();
     // Convert the current time to UTC
     let utcTime = initiateDate.toUTCString();
     // Trim the UTC time to remove seconds
     utcTime = utcTime.substring(0, utcTime.length - 7);
     // Insert the time on the calendar page
     document.querySelector(".navbar-nav.ml-auto .d-lg-block").insertAdjacentHTML("beforebegin", "<div class='utcTimeStamp'>" + "&nbsp;" + utcTime + " UTC" + "</div>");


 }